package no.uib.inf101.sem2.eventbus;

/**
 * @author Torstein Jarl Fagerbakke Strømme
 * Handle an event.
 * 
 * @param event the event to handle
 */
@FunctionalInterface
public interface EventHandler {

  void handle(IEvent event);
}
