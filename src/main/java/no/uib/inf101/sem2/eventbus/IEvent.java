package no.uib.inf101.sem2.eventbus;

/**
 * *@author Torstein Jarl Fagerbakke Strømme
 * An event that can be posted on the event bus. All events must
 * implement this interface in order to be posted on the event bus.
 */

public interface IEvent { }
