package no.uib.inf101.sem2.model;

/**
 * A weapon contains a name a action and a overpower list.
 * 
 * @param name   The name of the weapon
 * @param action The action of the weapon
 */

public record Weapon(String name, String action, OverpowersList overpowersList) {
    @Override
    public String toString() {
        return this.name;
    }
}
