package no.uib.inf101.sem2.model;

import java.util.ArrayList;
import java.util.List;

/**
 * creates a list of overpowered weapons. It has methods for adding weapons to
 * the list, getting the list of weapons, and checking if a weapon is in the
 * list.
 * 
 */
public class OverpowersList {
    private List<String> weapons = new ArrayList<>();

    public OverpowersList() {

    }

    public OverpowersList(ArrayList<String> list) {
        this.weapons = list;
    }

    // For looking through the weapon in this list.
    public Iterable<String> getWeapons() {
        return this.weapons;
    }

    public List<String> getList() {
        return this.weapons;
    }

    // Add a weaponName to the overpower list.
    public void addOverpowerWeapon(String weapon) {
        this.weapons.add(weapon);
    }

    // remove a weaponName from the overpower list.
    public void removeOverpowerWeapon(String weapon) {
        this.weapons.remove(weapon);
    }

    public boolean contains(String weaponName) {
        return this.weapons.contains(weaponName);
    }

}
