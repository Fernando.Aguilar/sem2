package no.uib.inf101.sem2.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import no.uib.inf101.sem2.eventbus.EventBus;

/**
 * creates a list of weapons and their associated overpowering weapons. It also
 * includes an EventBus to post events when a weapon is added to the list. The
 * class includes a constructor that takes an EventBus as a parameter, and a
 * constructor that creates a new EventBus. It also includes a method for adding
 * a weapon to the list, and a method for getting the list of weapons.
 */

public class WeaponList {

    private EventBus eventBus;
    OverpowersList rockList = new OverpowersList(new ArrayList<>(Arrays.asList("Scissors", "MagicWand")));
    OverpowersList scissorsList = new OverpowersList(new ArrayList<>(Arrays.asList("Paper", "Lizard")));
    OverpowersList paperList = new OverpowersList(new ArrayList<>(Arrays.asList("Rock", "MagicWand")));
    OverpowersList LizardList = new OverpowersList(new ArrayList<>(Arrays.asList("Paper", "Rock")));
    OverpowersList MagicWandList = new OverpowersList(new ArrayList<>(Arrays.asList("Lizard", "Scissors")));

    private Weapon Rock = new Weapon("Rock", "Crushes", rockList);
    private Weapon Scissors = new Weapon("Scissors", "Cuts", scissorsList);
    private Weapon Paper = new Weapon("Paper", "Covers", paperList);
    private Weapon Lizard = new Weapon("Lizard", "Eats", LizardList);
    private Weapon MagicWand = new Weapon("MagicWand", "disappear", MagicWandList);

    private List<Weapon> weapons = new ArrayList<>(Arrays.asList(Rock, Scissors, Paper, Lizard, MagicWand));

    /**
     * Create a new weapon list. The event bus is used to send events when a
     * weapon is added to the list.
     * 
     * @param eventBus the event bus to post events to.
     */
    public WeaponList(EventBus eventBus) {
        this.eventBus = eventBus;

    }

    // Create a new weapon list without an event bus.
    public WeaponList() {
        this(new EventBus());
        this.weapons.add(Rock);
        this.weapons.add(Scissors);
        this.weapons.add(Paper);
        this.weapons.add(Lizard);
        this.weapons.add(MagicWand);
    }

    // For looking through the weapon in this list.
    public Iterable<Weapon> getWeapons() {
        return this.weapons;
    }

    public List<Weapon> getList() {
        return this.weapons;
    }

    // Add a weapon to the list.
    public void addWeapon(Weapon weapon) {

        this.weapons.add(weapon);
        WeaponAddedToModelEvent event = new WeaponAddedToModelEvent(weapon);
        this.eventBus.post(event);
    }

    public void updateAll(Weapon updatedWeapon) {
        // find the index in weapons of the weapon that was updated
        int index = this.weapons.indexOf(updatedWeapon);
        // replace the weapon in weapons with the updated weapon
        this.weapons.set(index, updatedWeapon);
        // find wich weapon in the weapon list has an empty overpower list
        int emptyListIndex = 0;
        for (int i = 0; i < this.weapons.size(); i++) {
            if (this.weapons.get(i).overpowersList().getList().isEmpty()) {
                emptyListIndex = i;
            }
        }
        // update the overrpower list of that weapon (second Weapon created)
        Weapon weaponToUpdate = this.weapons.get(emptyListIndex);
        for (Weapon W : weapons) {
            if (W.name() != weaponToUpdate.name()) {
                if (!updatedWeapon.overpowersList().contains(W.name())) {
                    weaponToUpdate.overpowersList().addOverpowerWeapon(W.name());
                }
            }
        }
        this.weapons.set(emptyListIndex, weaponToUpdate);
        // update all the other overpower lists

        for (int w = 0; w < weapons.size(); w++) {
            for (int i = 0; i < weapons.size(); i++) {
                if (!weapons.get(i).overpowersList().contains(weapons.get(w).name())) {// if the weapon to be updated is
                                                                                       // not already in the
                                                                                       // overpowerlist of current
                                                                                       // weapon
                    if (!weapons.get(w).overpowersList().contains(weapons.get(i).name())) {// if the current weapon is
                                                                                           // not already in the
                                                                                           // overpowerlist of the
                                                                                           // weapon to be updated
                        if (weapons.get(w) != weapons.get(i)) {// if the current weapon is not the weapon to be updated
                            weapons.get(w).overpowersList().addOverpowerWeapon(weapons.get(i).name());
                        }
                    }
                }

            }
            this.weapons.set(w, weapons.get(w));
        }
        // post an event to update the view
        UpdateAllOverpowerListsEvent event = new UpdateAllOverpowerListsEvent(weapons);
        this.eventBus.post(event);

    }

}
