package no.uib.inf101.sem2.model;

import java.util.List;

import no.uib.inf101.sem2.eventbus.IEvent;

public record UpdateAllOverpowerListsEvent(List<Weapon> list) implements IEvent {

}
