package no.uib.inf101.sem2.model;

import java.util.Random;

import no.uib.inf101.sem2.eventbus.IEvent;

/**
 * This code is a class that implements the IEvent interface. It contains
 * methods to get a random weapon from a list, get the winner's weapon, get the
 * loser's weapon, and get the computer's weapon.
 */
public class playGame implements IEvent {

    private Weapon playersWeapon;
    private Weapon losersWeapon;
    private WeaponList weaponList;
    private Weapon computerWeapon;

    public playGame(Weapon playersWeapon, WeaponList weaponList) {
        this.playersWeapon = playersWeapon;
        this.weaponList = weaponList;
    }

    /**
     * This method gets a random weapon from the list of weapons. It then checks if
     * the player's weapon
     * overpowers the random weapon. If it does, the player's weapon is returned. If
     * it doesn't, the random
     * weapon is returned. If the random weapon is the same as the player's weapon,
     * null is returned.
     */
    public Weapon getWinnersWeapon() {
        Weapon randomWeapon = getRandomWeaponFromList();
        this.computerWeapon = randomWeapon;
        if (playersWeapon.overpowersList().contains(randomWeapon.name())) {
            this.losersWeapon = randomWeapon;
            return playersWeapon;

        } else if (randomWeapon == playersWeapon) {
            return null;
        } else {
            this.losersWeapon = playersWeapon;
            return randomWeapon;
        }

    }

    /**
     * This method returns the loser's weapon.
     */
    public Weapon getLosersWeapon() {
        return losersWeapon;
    }

    /**
     * This method returns a random weapon from the list of weapons.
     */
    public Weapon getRandomWeaponFromList() {
        // return a random weapon from the weaponList
        Random random = new Random();
        int randomIndex = random.nextInt(weaponList.getList().size());
        Weapon randomWeapon = weaponList.getList().get(randomIndex);
        return randomWeapon;
    }

    /**
     * This method returns the computer's weapon.
     */
    public Weapon getComputerWeapon() {
        return this.computerWeapon;
    }

}
