package no.uib.inf101.sem2.model;

import no.uib.inf101.sem2.eventbus.IEvent;

/**
 * @author Torstein Jarl Fagerbakke Strømme
 *         An event indicating that a weapon is added to the model.
 *         Event contains the weapon in question.
 */
public record WeaponAddedToModelEvent(Weapon weapon) implements IEvent {

}