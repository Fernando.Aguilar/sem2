package no.uib.inf101.sem2.view;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.model.playGame;

/**
 * This class is the view for the game play.
 * It displays the players weapon, the computers weapon and the winner.
 */
public class ViewPlayGame implements IEvent {

    private JPanel mainPanel;
    private playGame PlayGame;
    private Weapon playersWeapon;
    private Weapon computerWeapon;
    private Weapon winnerWeapon;
    private Weapon loserWeapon;

    public ViewPlayGame(Weapon playersWeapon, EventBus eventBus, WeaponList weapons) {
        this.mainPanel = new JPanel();
        this.PlayGame = new playGame(playersWeapon, weapons);
        this.playersWeapon = playersWeapon;

        this.winnerWeapon = PlayGame.getWinnersWeapon();
        this.loserWeapon = PlayGame.getLosersWeapon();

        this.computerWeapon = PlayGame.getComputerWeapon();
        mainPanel.setLayout(new GridLayout(5, 1));
        this.mainPanel.setPreferredSize(new Dimension(200, 200));

        if (winnerWeapon == null) {
            this.computerWeapon = this.playersWeapon;
        }
        JLabel PlayersWeaponLabel = new JLabel("Players Weapon: " + playersWeapon.name());
        PlayersWeaponLabel.setBounds(50, 50, 100, 30);
        PlayersWeaponLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
        this.mainPanel.add(PlayersWeaponLabel);
        this.mainPanel.add(new ViewWeaponData(playersWeapon).getImage());

        JLabel ComputerWeaponLabel = new JLabel("Computers Weapon: " + computerWeapon.name());
        ComputerWeaponLabel.setBounds(50, 50, 100, 30);
        ComputerWeaponLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
        this.mainPanel.add(ComputerWeaponLabel);
        this.mainPanel.add(new ViewWeaponData(computerWeapon).getImage());

        if (winnerWeapon == null) {
            JLabel ItsATieLabel = new JLabel("It's a tie!");
            ItsATieLabel.setBounds(50, 50, 100, 30);
            ItsATieLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
            this.mainPanel.add(ItsATieLabel);

        } else {

            JLabel WinnerLable = new JLabel(
                    winnerWeapon.name() + " " + winnerWeapon.action() + " " + loserWeapon.name());
            WinnerLable.setBounds(50, 50, 100, 30);
            WinnerLable.setFont(new Font("Verdana", Font.PLAIN, 18));
            this.mainPanel.add(WinnerLable);

            JLabel loseLabel = new JLabel("You LOSE!");
            loseLabel.setBounds(50, 50, 100, 30);
            loseLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
            JLabel winLabel = new JLabel("You WIN!");
            winLabel.setBounds(50, 50, 100, 30);
            winLabel.setFont(new Font("Verdana", Font.PLAIN, 18));
            if (playersWeapon == winnerWeapon) {
                this.mainPanel.add(winLabel);
            } else {
                this.mainPanel.add(loseLabel);
            }

        }

    }

    public Weapon getWinnnersWeapon() {
        return this.winnerWeapon;
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

}
