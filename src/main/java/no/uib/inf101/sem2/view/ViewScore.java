package no.uib.inf101.sem2.view;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class is the view for the score.
 * @param playerScore
 * @param computerScore
 */
public class ViewScore {

    private JPanel scorePanel;

    public ViewScore(int playerScore, int computerScore) {

        this.scorePanel = new JPanel();
        this.scorePanel.setLayout(new BoxLayout(scorePanel, BoxLayout.PAGE_AXIS));
        this.scorePanel.add(new JLabel("Score"));
        this.scorePanel.add(new JLabel("Player Wins: " + playerScore));
        this.scorePanel.add(new JLabel("Computer Wins: " + computerScore));

    }

    public JPanel getScorePanel() {
        return scorePanel;
    }

}
