package no.uib.inf101.sem2.view;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import no.uib.inf101.sem2.model.OverpowersList;

public class ViewOverpowersList {

    /**
     * This class is the view for the list of overpowers.
     */
    private OverpowersList list;
    JScrollPane mainPanel;

    public ViewOverpowersList(OverpowersList list) {

        this.list = list;

        // make a list of overpowers and add it to the main panel
        DefaultListModel<String> List = new DefaultListModel<>();
        JList<String> overpowersList = new JList<>(List);
        for (String weapon : list.getWeapons()) {
            List.addElement(weapon);
        }

        this.mainPanel = new JScrollPane(overpowersList);

    }

    public JComponent getMainPanel() {
        return this.mainPanel;
    }

    public OverpowersList getList() {
        return this.list;
    }
}
