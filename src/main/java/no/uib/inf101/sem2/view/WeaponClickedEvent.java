package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.Weapon;

/**
 * An event that is posted when a weapon is clicked. Contains the weapon
 * that was clicked.
 */
public record WeaponClickedEvent(Weapon weaponClicked) implements IEvent {

    public Weapon getWeapon() {
        return this.weaponClicked;
    }
}