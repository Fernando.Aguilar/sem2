package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponList;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class mainView {
  /**
   * This method creates the main window. It is the main window that the
   * user interacts with. It contains a right panel, a center panel and a
   * left panel.
   * 
   * @param model    the model for the weapons
   * @param eventBus the event bus to post and listen to events
   * @return the main window
   * 
   * 
   */
  private EventBus eventBus;

  private JPanel mainPanel;
  private JPanel rightPanel;
  private JPanel centerPanel;
  private JPanel leftPanel;

  private WeaponList weapons;

  private Weapon playersWeapon;
  private Weapon winnersWeapon;
  private Weapon weaponToUpdate;

  private Integer playerScore;
  private int computerScore;

  private JButton buttonCreate;
  private JButton buttonPlay;

  private enum RigthPanelViewState {
    NONE, CREATE, DATA, DISABLE
  }

  private RigthPanelViewState rightPanelViewState = RigthPanelViewState.NONE;

  public mainView(WeaponList weapons, EventBus eventBus) {

    this.eventBus = eventBus;
    this.weapons = weapons;
    this.playerScore = 0;
    this.computerScore = 0;

    this.mainPanel = new JPanel();
    this.mainPanel.setPreferredSize(new Dimension(1000, 700));
    this.mainPanel.setLayout(new BorderLayout(30, 30));
    this.mainPanel.setBorder(new EmptyBorder(15, 15, 15, 15));

    this.rightPanel = new JPanel();
    this.centerPanel = new JPanel();
    this.leftPanel = new JPanel();

    this.mainPanel.add(this.rightPanel, BorderLayout.EAST);
    this.mainPanel.add(this.centerPanel, BorderLayout.CENTER);
    this.mainPanel.add(this.leftPanel, BorderLayout.WEST);

    // Button to go to view to create a new weapon
    this.buttonCreate = new JButton("Create new Weapon");
    buttonCreate.addActionListener(e -> {
      ViewCreateNewWeapon createWeaponView = new ViewCreateNewWeapon(this.eventBus);
      refreshRightPanelContent(createWeaponView.getMainPanel());
      this.rightPanelViewState = RigthPanelViewState.DISABLE;
      buttonCreate.setEnabled(false);
    });

    this.mainPanel.add(buttonCreate, BorderLayout.NORTH);

    // button to play the game
    this.buttonPlay = new JButton("Play");
    JButton buttonPlayAgain = new JButton("Play Again");
    buttonPlay.addActionListener(e -> {
      ViewPlayGame playGameView = new ViewPlayGame(playersWeapon, this.eventBus, weapons);
      this.winnersWeapon = playGameView.getWinnnersWeapon();
      refreshCenterPanelContent(playGameView.getMainPanel());
      refreshRightPanelContent(buttonPlayAgain);
      handleScore(winnersWeapon);
      refreshLeftPanelContent(new ViewScore(playerScore, computerScore).getScorePanel());
      buttonPlay.setEnabled(false);
    });
    this.mainPanel.add(buttonPlay, BorderLayout.SOUTH);
    buttonPlay.setEnabled(false);

    // button to play the game Again
    buttonPlayAgain.addActionListener(e -> {
      refreshCenterPanelContent(new ViewListOfWeapons(weapons, eventBus).getMainPanel());
      refreshRightPanelContent(new ViewWeaponData(null).getMainPanel());
      this.rightPanelViewState = RigthPanelViewState.NONE;
      buttonPlay.setEnabled(true);
    });

    this.setupLeftPanel();
    this.setupcenterPanel(weapons);
    this.setupRightPanel();

  }

  private void setupLeftPanel() {
    this.leftPanel.setLayout(new BorderLayout());
    ViewScore viewScore = new ViewScore(this.playerScore, this.computerScore);
    this.leftPanel.add(viewScore.getScorePanel());

  }

  private void setupcenterPanel(WeaponList weapons) {
    this.centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.PAGE_AXIS));

    // List of Weapons
    ViewListOfWeapons viewLisOfWeapons = new ViewListOfWeapons(weapons, eventBus);
    this.centerPanel.add(viewLisOfWeapons.getMainPanel());

  }

  private void setupRightPanel() {
    this.rightPanel.setLayout(new BorderLayout());
    ViewWeaponData viewWeaponData = new ViewWeaponData(null);
    this.rightPanel.add(viewWeaponData.getMainPanel(), BorderLayout.CENTER);
    this.eventBus.register(this::handleWeaponClickedEvent);
    this.eventBus.register(this::handleWeaponCreatedEvent);
    this.eventBus.register(this::handleUpdateOpverpowerListEvent);
  }

  private void handleScore(Weapon winnersWeapon) {
    if (winnersWeapon == null) {
      // do nothing
    } else if (playersWeapon == winnersWeapon) {
      playerScore++;
    } else {
      computerScore++;
    }

  }

  /**
   * This method is called when the user clicks on the create button in the
   * create weapon view.
   */
  private void handleWeaponCreatedEvent(IEvent event) {
    if (event instanceof WeaponCreatedEvent weaponCreatedEvent) {
      this.weaponToUpdate = new Weapon(weaponCreatedEvent.name(), weaponCreatedEvent.action(),
          weaponCreatedEvent.list());
      int WeaponListFull = ((weapons.getList().size() - 1) / 2);
      ViewOverpowerListUpdate viewOverpowerListUpdate = new ViewOverpowerListUpdate(weaponToUpdate, eventBus,
          WeaponListFull);
      refreshRightPanelContent(viewOverpowerListUpdate.getMainPanel());

      this.rightPanelViewState = RigthPanelViewState.CREATE;

    }
  }

  /**
   * This method is called when the user clicks on a weapon in the list of
   * weapons. It will update the right panel with the data of the weapon clicked
   * on
   */
  private void handleWeaponClickedEvent(IEvent event) {
    if (event instanceof WeaponClickedEvent weaponClickedEvent) {
      switch (rightPanelViewState) {
        case NONE:

        case DATA:
          rightPanelViewState = RigthPanelViewState.DATA;
          ViewWeaponData viewWeaponData = new ViewWeaponData(weaponClickedEvent.weaponClicked());
          refreshRightPanelContent(viewWeaponData.getMainPanel());
          this.playersWeapon = weaponClickedEvent.weaponClicked();
          buttonPlay.setEnabled(true);

          break;
        case CREATE:
          handleUpdateOpverpowerList(weaponClickedEvent.weaponClicked());
          int WeaponListFull = ((weapons.getList().size() - 1) / 2);
          ViewOverpowerListUpdate viewOverpowerListUpdate = new ViewOverpowerListUpdate(weaponToUpdate, eventBus,
              WeaponListFull);
          refreshRightPanelContent(viewOverpowerListUpdate.getMainPanel());

          break;
        case DISABLE:
          // do nothing
          break;
      }

    }

  }

  /**
   * This method is used to update the list of overpower weapons
   */
  private void handleUpdateOpverpowerList(Weapon weaponClicked) {
    int ListSize = ((weapons.getList().size() - 1) / 2);

    if (weaponClicked.name() != weaponToUpdate.name()) {
      if (!weaponToUpdate.overpowersList().contains(weaponClicked.name())) {
        weaponToUpdate.overpowersList().addOverpowerWeapon(weaponClicked.name());
      }
    }
    if (weaponToUpdate.overpowersList().getList().size() > ListSize) {
      weaponToUpdate.overpowersList().removeOverpowerWeapon(weaponToUpdate.overpowersList().getList().get(0));
    }
  }

  private void refreshRightPanelContent(JComponent view) {
    this.rightPanel.removeAll();
    this.rightPanel.add(view);
    this.mainPanel.revalidate();
    this.mainPanel.repaint();
  }

  private void refreshCenterPanelContent(JComponent view) {
    this.centerPanel.removeAll();
    this.centerPanel.add(view);
    this.mainPanel.revalidate();
    this.mainPanel.repaint();
  }

  private void refreshLeftPanelContent(JComponent view) {
    this.leftPanel.removeAll();
    this.leftPanel.add(view);
    this.mainPanel.revalidate();
    this.mainPanel.repaint();
  }

  /**
   * This method is used to update the list of overpower weapons
   */
  private void handleUpdateOpverpowerListEvent(IEvent ievent1) {
    if (ievent1 instanceof UpdateAllEvent UpdateAllEvent) {
      this.rightPanelViewState = RigthPanelViewState.DATA;
      ViewWeaponData viewWeaponData = new ViewWeaponData(null);
      refreshRightPanelContent(viewWeaponData.getMainPanel());
      // refreshCenterPanelContent(centerPanel);
      buttonCreate.setEnabled(true);

    }
  }

  /** Get the main panel of this view */
  public JPanel getMainPanel() {
    return this.mainPanel;
  }
}
