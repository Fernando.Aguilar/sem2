package no.uib.inf101.sem2.view;

import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.model.Weapon;

/**
 * This class creates the panel for updating the overpower list of the weapon.
 * @param weaponToUpdate
 * @param eventBus
 * @param WeaponListFull
 * @return mainPanel
 * 
 */
public class ViewOverpowerListUpdate {

    private JPanel mainPanel;
    private ViewOverpowersList viewOverpowersList;
    private Weapon weaponToUpdate;

    public ViewOverpowerListUpdate(EventBus eventBus) {

    }

    public ViewOverpowerListUpdate(Weapon weaponToUpdate, EventBus eventBus, int WeaponListFull) {

        this.weaponToUpdate = weaponToUpdate;
        this.mainPanel = new JPanel();
        this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
        this.mainPanel.add(new JLabel(this.weaponToUpdate.name() + "  " + this.weaponToUpdate.action()));
        this.viewOverpowersList = new ViewOverpowersList(weaponToUpdate.overpowersList());
        this.mainPanel.add(viewOverpowersList.getMainPanel());

        JButton updateButton = new JButton("Update");
        mainPanel.add(updateButton);
        updateButton.setEnabled(false);

        if (this.weaponToUpdate.overpowersList().getList().size() == WeaponListFull) {
            updateButton.setEnabled(true);
        }
        ActionListener lisener = (e -> {
            eventBus.post(new UpdateAllEvent(this.weaponToUpdate));
        });

        updateButton.addActionListener(lisener);

    }

    public JComponent getMainPanel() {
        return mainPanel;
    }

}
