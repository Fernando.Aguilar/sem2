package no.uib.inf101.sem2.view;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.UpdateAllOverpowerListsEvent;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponAddedToModelEvent;
import no.uib.inf101.sem2.model.WeaponList;

/**
 * This class is the view for the list of weapons. It is a list of weapons
 * that the user can click on to see more information about the weapon.
 * 
 * @param weapons  the model for the weapons
 * @param eventBus the event bus to post and listen to events
 */
public class ViewListOfWeapons {

  private JScrollPane mainPane;
  private DefaultListModel<Weapon> viewableList;
  private JList<Weapon> listView;

  public ViewListOfWeapons(WeaponList weapons, EventBus eventBus) {
    this.viewableList = new DefaultListModel<>();
    this.listView = new JList<>(viewableList);
    this.mainPane = new JScrollPane(listView);

    // Load the viewable list with data from the model
    for (Weapon weapon : weapons.getWeapons()) {
      viewableList.addElement(weapon);
    }

    // set up the event bus
    eventBus.register(this::updateListToViewWhenModelChanges);
    eventBus.register(this::updateListWhenOverpowerUpdated);
    this.setupMouseClickEvents(listView, eventBus);
  }

  // Set up the mouse click events for the list view of weapons.
  private void setupMouseClickEvents(final JList<Weapon> listView, final EventBus eventBus) {
    listView.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent evt) {
        if (evt.getClickCount() == 1) {
          Weapon weapon = listView.getSelectedValue();
          eventBus.post(new WeaponClickedEvent(weapon));
        }
      }
    });
  }

  // Update the view when the model changes (a new weapon is added)

  private void updateListToViewWhenModelChanges(IEvent e) {
    if (e instanceof WeaponAddedToModelEvent event) {
      Weapon weapon = event.weapon();
      this.viewableList.addElement(weapon);
    }
  }

  /** Get the main panel of this view. */
  public JComponent getMainPanel() {
    return this.mainPane;
  }

  /*
   * Update the list of weapons when the list of overpowering weapons is updated.
   */
  private void updateListWhenOverpowerUpdated(IEvent ievent1) {
    if (ievent1 instanceof UpdateAllOverpowerListsEvent event) {
      List<Weapon> weaponsList = event.list();
      this.viewableList.removeAllElements();
      for (Weapon weapon : weaponsList) {
        viewableList.addElement(weapon);
      }
    }
  }

}
