package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.Weapon;

public record UpdateAllEvent(Weapon weapon) implements IEvent {

}
