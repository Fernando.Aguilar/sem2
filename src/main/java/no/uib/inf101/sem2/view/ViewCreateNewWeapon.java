package no.uib.inf101.sem2.view;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.model.OverpowersList;

public class ViewCreateNewWeapon {

  private JPanel mainPanel;
  private JPanel NameAndActionPanel;

  private OverpowersList list_1 = new OverpowersList();
  private OverpowersList list_2 = new OverpowersList();
  private EventBus eventBus;

  /**
   * ViewCreateNewWeapon this is the view for creating a new weapon.
   * 
   * @param eventBus
   * @return mainPanel
   */
  public ViewCreateNewWeapon(EventBus eventBus) {

    this.mainPanel = new JPanel();
    this.mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
    this.eventBus = eventBus;
    this.NameAndActionPanel = new JPanel();

    this.mainPanel.add(NameAndActionPanel);
    this.setupOverPanel();

  }

  /**
   * This method creates the panel for the name and action of the weapon.
   * It also creates the textfields for the name and action.
   */
  private void setupOverPanel() {
    this.NameAndActionPanel.setLayout(new GridBagLayout());

    GridBagConstraints gbc = new GridBagConstraints();

    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 0;
    gbc.gridwidth = 2;
    this.NameAndActionPanel.add(new JLabel("Weapon Name 1:"), gbc);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.gridx = 0;
    gbc.gridy = 1;
    gbc.gridwidth = 2;
    JTextField nameField_1 = new JTextField();
    this.NameAndActionPanel.add(nameField_1, gbc);

    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 2;
    gbc.gridwidth = 2;
    this.NameAndActionPanel.add(new JLabel("action:"), gbc);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 3;
    gbc.gridwidth = 2;
    JTextField actionField_1 = new JTextField();
    this.NameAndActionPanel.add(actionField_1, gbc);

    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 4;
    gbc.gridwidth = 2;
    this.NameAndActionPanel.add(new JLabel("Weapon Name 2:"), gbc);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 5;
    gbc.gridwidth = 2;
    JTextField nameField_2 = new JTextField();
    this.NameAndActionPanel.add(nameField_2, gbc);

    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 6;
    gbc.gridwidth = 2;
    this.NameAndActionPanel.add(new JLabel("action:"), gbc);
    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 7;
    gbc.gridwidth = 2;
    JTextField actionField_2 = new JTextField();
    this.NameAndActionPanel.add(actionField_2, gbc);

    gbc.fill = GridBagConstraints.HORIZONTAL;
    gbc.ipady = 15;
    gbc.gridx = 0;
    gbc.gridy = 8;
    gbc.gridwidth = 2;
    JButton createButton = new JButton("Create weapon");
    this.NameAndActionPanel.add(createButton, gbc);

    ActionListener listener_1 = (e) -> {
      String name_1 = nameField_1.getText();

      String action_1 = actionField_1.getText();
      String name_2 = nameField_2.getText();

      String action_2 = actionField_2.getText();
      eventBus.post(new WeaponCreatedEvent(name_2, action_2, list_2));
      eventBus.post(new WeaponCreatedEvent(name_1, action_1, list_1));

    };
    createButton.addActionListener(listener_1);

  }

  /** Get the main panel for this component */
  public JPanel getMainPanel() {
    return this.mainPanel;
  }
}
