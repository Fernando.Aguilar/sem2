package no.uib.inf101.sem2.view;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import no.uib.inf101.sem2.model.Weapon;

/**
 * This class is the view for the weapon data.
 * @param weapon
 */
public class ViewWeaponData extends Canvas {

    private JPanel mainPanel;
    private JLabel Image;

    public ViewWeaponData(Weapon weapon) {
        this.mainPanel = new JPanel();
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));

        this.mainPanel.setPreferredSize(new Dimension(200, 200));

        if (weapon == null) {
            this.mainPanel.add(new JLabel("no Weapon selected"));
            return;
        }
        String fileName = weapon.name() + ".gif";
        if (!fileName.startsWith("/")) {
            fileName = "/" + fileName;
            InputStream is = Inf101Graphics.class.getResourceAsStream(fileName);

            if (is != null) {
                BufferedImage imgWeapon = Inf101Graphics.loadImageFromResources(weapon.name() + ".gif");
                ImageIcon icon = new ImageIcon(imgWeapon);
                Image image = icon.getImage(); // transform it
                Image newimg = image.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); // scale it the smooth way
                icon = new ImageIcon(newimg); // transform it back
                JLabel iconLabel = new JLabel(icon);
                this.Image = iconLabel;
                this.mainPanel.add(iconLabel);
            } else {
                JLabel errorLabel = new JLabel("image not found ");
                this.Image = errorLabel;
                this.mainPanel.add(errorLabel);

            }

            JLabel nameLabel = new JLabel(weapon.name());
            JLabel actionLabel = new JLabel(weapon.action());

            this.mainPanel.add(nameLabel);
            this.mainPanel.add(actionLabel);

            ViewOverpowersList view = new ViewOverpowersList(weapon.overpowersList());
            this.mainPanel.add(view.mainPanel);

        }
    }

    public JLabel getImage() {
        return Image;
    }

    public JPanel getMainPanel() {
        return this.mainPanel;
    }

}
