package no.uib.inf101.sem2.view;

import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.OverpowersList;

/**
 * An event that is posted when a weapon is created. Contains the name and
 * action of the weapon
 * that was created.
 */
public record WeaponCreatedEvent(String name, String action, OverpowersList list) implements IEvent {

}
