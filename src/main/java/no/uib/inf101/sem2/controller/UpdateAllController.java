package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.view.UpdateAllEvent;

/**
 * A controller that updates all weapons in the model when a UpdateAllEvent is
 * posted to the event bus.
 */
public class UpdateAllController {

    private final WeaponList model;

    /**
     * Create a new controller that updates all weapons in the model when a
     * UpdateAllEvent is posted to the event bus.
     *
     * @param model    The model to update
     * @param eventBus The event bus to listen to
     */
    public UpdateAllController(WeaponList model, EventBus eventBus) {
        this.model = model;
        eventBus.register(this::reactToUpdateAllEvents);

    }

    private void reactToUpdateAllEvents(IEvent e) {
        if (e instanceof UpdateAllEvent event) {
            Weapon newWeapon = new Weapon(event.weapon().name(), event.weapon().action(),
                    event.weapon().overpowersList());
            this.model.updateAll(newWeapon);
        }
    }
}
