package no.uib.inf101.sem2.controller;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.eventbus.IEvent;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.view.WeaponCreatedEvent;

/**
 * A controller that adds a weapon to the model when a WeaponCreatedEvent is
 * posted to the event bus.
 */
public class AddWeaponController {

  private final WeaponList model;

  /**
   * Create a new controller that adds a weapon to the model when a
   * WeaponCreatedEvent is posted to the event bus.
   *
   * @param model    The model to add the weapon to
   * @param eventBus The event bus to listen to
   */
  public AddWeaponController(WeaponList model, EventBus eventBus) {
    this.model = model;
    eventBus.register(this::reactToWeaponCreatedEvents);
  }

  private void reactToWeaponCreatedEvents(IEvent e) {
    if (e instanceof WeaponCreatedEvent event) {
      Weapon newWeapon = new Weapon(event.name(), event.action(), event.list());
      this.model.addWeapon(newWeapon);
    }
  }

}
