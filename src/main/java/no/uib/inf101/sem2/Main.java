package no.uib.inf101.sem2;

import no.uib.inf101.sem2.controller.AddWeaponController;
import no.uib.inf101.sem2.controller.UpdateAllController;
import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.view.mainView;

import javax.swing.JFrame;

/**
 * This class is the main class for the program.
 */
public class Main {
  public static void main(String[] args) {
    EventBus eventBus = new EventBus();
    WeaponList weapons = new WeaponList(eventBus);
    mainView view = new mainView(weapons, eventBus);
    new AddWeaponController(weapons, eventBus);
    new UpdateAllController(weapons, eventBus);

    JFrame frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("INF101_Sem2    Advanced Rock Paper Scissors");
    frame.setContentPane(view.getMainPanel());
    frame.pack();
    frame.setVisible(true);
  }
}
