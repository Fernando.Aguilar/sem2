package no.uib.inf101.sem2.ModelTests;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.OverpowersList;

public class OverpowersListTest {
    
	@Test
	void testAddOverpowerWeapon() {
		OverpowersList list = new OverpowersList();
		list.addOverpowerWeapon("sword");
		assertTrue(list.contains("sword"));
	}
	
	@Test
	void testRemoveOverpowerWeapon() {
		OverpowersList list = new OverpowersList();
		list.addOverpowerWeapon("sword");
		list.removeOverpowerWeapon("sword");
		assertFalse(list.contains("sword"));
	}
	
	@Test
	void testContains() {
		OverpowersList list = new OverpowersList();
		list.addOverpowerWeapon("sword");
		assertTrue(list.contains("sword"));
		assertFalse(list.contains("axe"));
	}

}