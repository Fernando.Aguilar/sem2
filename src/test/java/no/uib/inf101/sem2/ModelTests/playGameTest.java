package no.uib.inf101.sem2.ModelTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.model.OverpowersList;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.model.playGame;

public class playGameTest {

	@Test
	public void testGetWinnersWeapon() {
		WeaponList weaponList = new WeaponList();
        OverpowersList rockList = new OverpowersList(new ArrayList<>(Arrays.asList("Scissors", "MagicWand")));
		Weapon playersWeapon = new Weapon("Rock", "Crushes", rockList);
		playGame game = new playGame(playersWeapon, weaponList);
		
		Weapon winnersWeapon = game.getWinnersWeapon();
		
		assertEquals(playersWeapon, winnersWeapon);
	}
	
	@Test
	public void testGetLosersWeapon() {
		WeaponList weaponList = new WeaponList();
        OverpowersList rockList = new OverpowersList(new ArrayList<>(Arrays.asList("Scissors", "MagicWand")));
		Weapon playersWeapon = new Weapon("Rock", "Crushes", rockList);
		playGame game = new playGame(playersWeapon, weaponList);
		
		Weapon losersWeapon = game.getLosersWeapon();
		
		assertEquals(null, losersWeapon);
	}
	
	@Test
	public void testGetRandomWeaponFromList() {
		WeaponList weaponList = new WeaponList();
        OverpowersList rockList = new OverpowersList(new ArrayList<>(Arrays.asList("Scissors", "MagicWand")));
		Weapon playersWeapon = new Weapon("Rock", "Crushes", rockList);
		playGame game = new playGame(playersWeapon, weaponList);
		
		Weapon randomWeapon = game.getRandomWeaponFromList();
		
		assertNotNull(randomWeapon);
	}
	
	@Test
	public void testGetComputerWeapon() {
		WeaponList weaponList = new WeaponList();
        OverpowersList rockList = new OverpowersList(new ArrayList<>(Arrays.asList("Scissors", "MagicWand")));
		Weapon playersWeapon = new Weapon("Rock", "Crushes", rockList);
		playGame game = new playGame(playersWeapon, weaponList);
		
		Weapon computerWeapon = game.getComputerWeapon();
		
		assertNull(computerWeapon);
	}

}