package no.uib.inf101.sem2.ModelTests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.model.OverpowersList;
import no.uib.inf101.sem2.model.Weapon;
import no.uib.inf101.sem2.model.WeaponList;

class WeaponListTest {

    @Test
    void testConstructor() {
        EventBus eventBus = new EventBus();
        WeaponList weaponList = new WeaponList(eventBus);
        assertNotNull(weaponList);
        assertNotNull(weaponList.getList());
        assertEquals(5, weaponList.getList().size());

        Weapon rock = weaponList.getList().get(0);
        assertEquals("Rock", rock.name());
        assertEquals("Crushes", rock.action());
        assertEquals(2, rock.overpowersList().getList().size());
        assertTrue(rock.overpowersList().contains("Scissors"));
        assertTrue(rock.overpowersList().contains("MagicWand"));

        Weapon scissors = weaponList.getList().get(1);
        assertEquals("Scissors", scissors.name());
        assertEquals("Cuts", scissors.action());
        assertEquals(2, scissors.overpowersList().getList().size());
        assertTrue(scissors.overpowersList().contains("Paper"));
        assertTrue(scissors.overpowersList().contains("Lizard"));

        Weapon paper = weaponList.getList().get(2);
        assertEquals("Paper", paper.name());
        assertEquals("Covers", paper.action());
        assertEquals(2, paper.overpowersList().getList().size());
        assertTrue(paper.overpowersList().contains("Rock"));
        assertTrue(paper.overpowersList().contains("MagicWand"));

        Weapon lizard = weaponList.getList().get(3);
        assertEquals("Lizard", lizard.name());
        assertEquals("Eats", lizard.action());
        assertEquals(2, lizard.overpowersList().getList().size());
        assertTrue(lizard.overpowersList().contains("Paper"));
        assertTrue(lizard.overpowersList().contains("Rock"));

        Weapon magicWand = weaponList.getList().get(4);
        assertEquals("MagicWand", magicWand.name());
        assertEquals("disappear", magicWand.action());
        assertEquals(2, magicWand.overpowersList().getList().size());
        assertTrue(magicWand.overpowersList().contains("Lizard"));
        assertTrue(magicWand.overpowersList().contains("Scissors"));
    }

    @Test
    void testAddWeapon() {
        EventBus eventBus = new EventBus();
        WeaponList weaponList = new WeaponList(eventBus);
        Weapon sword = new Weapon("Sword", "Slices",
                new OverpowersList(new ArrayList<>(Arrays.asList("Rock", "Paper", "Lizard"))));
        Weapon Glock = new Weapon("Sword", "Slices", new OverpowersList(new ArrayList<>(Arrays.asList())));
        weaponList.addWeapon(sword);
        weaponList.addWeapon(Glock);
        assertEquals(7, weaponList.getList().size());
        assertTrue(weaponList.getList().contains(sword));

    }
}
