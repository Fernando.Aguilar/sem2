package no.uib.inf101.sem2.ViewTests;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.view.mainView;


import static org.junit.jupiter.api.Assertions.assertNotNull;


public class mainViewTest {

    @Test
    public void testMainView() {
        WeaponList weapons = new WeaponList();
        EventBus eventBus = new EventBus();
        mainView view = new mainView(weapons, eventBus);
        assertNotNull(view);
    }
}