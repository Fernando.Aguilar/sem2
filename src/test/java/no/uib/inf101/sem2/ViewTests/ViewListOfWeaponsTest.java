package no.uib.inf101.sem2.ViewTests;


import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.eventbus.EventBus;

import no.uib.inf101.sem2.model.WeaponList;
import no.uib.inf101.sem2.view.ViewListOfWeapons;


public class ViewListOfWeaponsTest {
  
    @Test
    public void testViewListOfWeapons() {
        WeaponList weapons = new WeaponList();
        EventBus eventBus = new EventBus();
        ViewListOfWeapons view = new ViewListOfWeapons(weapons, eventBus);
        assertNotNull(view);
    }
}