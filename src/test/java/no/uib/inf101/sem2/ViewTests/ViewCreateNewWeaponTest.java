package no.uib.inf101.sem2.ViewTests;


import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

import no.uib.inf101.sem2.eventbus.EventBus;
import no.uib.inf101.sem2.view.ViewCreateNewWeapon;

public class ViewCreateNewWeaponTest {
    @Test
    void testViewCreateNewWeapon() {
        EventBus eventBus = new EventBus();
        ViewCreateNewWeapon view = new ViewCreateNewWeapon(eventBus);
        assertNotNull(view);
    }    
}