# Advance Rock Paper Scissors Game

Rock, Paper and Scissors is a classic game that has been around for generations. It is a game of chance and strategy, where players choose one of three options - rock, paper or scissors - and try to beat their opponent.

This game is an advanced version of the Rock Paper Scissors game. In this game, it is posible to play with more that tree weapons. A player chosses a Weapon from a List of weapons and the Weapon data is displayed in the screen. The player can choose to play against the computer or against another player (not time to implement, in this version it is only posible to play agains the computer). The weapons are compared and one weapon is determined to be the winner, a counter tracks the wins and The player who wins the most rounds wins the game.

This game also offers the possibility to create and customize new weapons. Players can create two weapons at a time, each with different actions, and then choose which weapons, the newly created weapon, will overpower. After creating the new weapons and deciding which weapons will overpower, all the other weapons are updated with the new weapons and the new weapons are added to the list of weapons.

To manage the complexity of this program, a Model-View-Controller (MVC) approach coupled with a bus was used. This approach separates each part of the game, allowing them to be worked on and tested independently, while the bus allows the different parts of the game to communicate with each other. The Model manages the data, the View manages the user interface, and the Controller manages the logic of the game. However, much of the logic in this game is managed by both the View and the Model.

Video Demo: https://youtu.be/QwqABvKb4rM



 